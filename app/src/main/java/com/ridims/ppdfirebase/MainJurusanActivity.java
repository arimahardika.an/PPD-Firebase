package com.ridims.ppdfirebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ridims.ppdfirebase.Helper.Static;

public class MainJurusanActivity extends AppCompatActivity {


    //TODO : UI PART : Add simple splash screen before this page
    //TODO : UX PART : Add loading screen when loading data

    private ListView jurusanListView;
    private DatabaseReference rootref = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference jurusanref = rootref.child("listjurusan");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jurusan_main);

        jurusanListView = (ListView) findViewById(R.id.list_jurusan);

        initFirebase();

    }

    private void initFirebase() {
        FirebaseListAdapter<String> jurusanAdapter = new FirebaseListAdapter<String>(this, String.class,
                android.R.layout.simple_list_item_1, jurusanref) {
            @Override
            protected void populateView(View v, String model, final int position) {
                ((TextView) v.findViewById(android.R.id.text1)).setText(model);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String val = getItem(position);
                        String key = getRef(position).getKey();

                        Intent intentJurusan = new Intent(MainJurusanActivity.this, ListDosenActivity.class);
                        intentJurusan.putExtra(Static.getJurusanKey(), val);
                        startActivity(intentJurusan);

                        System.out.println("ini valuenya " + val);
                        System.out.println("ini keynya : " + key);
                    }
                });
            }
        };

        jurusanListView.setAdapter(jurusanAdapter);
    }
}
