package com.ridims.ppdfirebase;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ridims.ppdfirebase.Helper.DetailModel;
import com.ridims.ppdfirebase.Helper.Static;

/**
 * Created by
 * Name : Ari Mahardika Ahmad Nafis
 * Email : arimahardika.an@gmail.com
 * Git : gitlab.com/Ari.Mahardika
 * Date : 17/10/16
 */

public class DetailActivity extends AppCompatActivity {

    private String KEY_DOSEN;
    private String KEY_JURUSAN;

    private String EMAIL;
    private String NO_HP;

    private TextView nama, nohp, email, alamat, notelprumah, ruang;
    private ImageButton btn_call, btn_email;

    private LinearLayout ll_email, ll_call;

    private DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent i = getIntent();
        KEY_DOSEN = i.getStringExtra(Static.getDosenKey());
        KEY_JURUSAN = i.getStringExtra(Static.getJurusanKey());

        initLayout();

        initFirebase();

        emailAndCallAction();

    }

    private void emailAndCallAction() {
        ll_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:" + EMAIL));
                startActivity(Intent.createChooser(emailIntent, "Send Email"));
            }
        });

        ll_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Custom Dialog
                final Dialog chooseDialog = new Dialog(DetailActivity.this);
                chooseDialog.setContentView(R.layout.dialog_call_or_message);
                chooseDialog.setTitle("Chooser");

                chooseDialog.show();

                LinearLayout dialogMessage = (LinearLayout) chooseDialog.findViewById(R.id.dialog_message);
                dialogMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        chooseDialog.dismiss();
                        Uri uri = Uri.parse("smsto:" + NO_HP);
                        Intent sms = new Intent(Intent.ACTION_SENDTO, uri);
                        startActivity(sms);
                    }
                });
                LinearLayout dialogCall = (LinearLayout) chooseDialog.findViewById(R.id.dialog_call);
                dialogCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        chooseDialog.dismiss();
                        Intent call = new Intent(Intent.ACTION_DIAL);
                        call.setData(Uri.parse("tel:" + NO_HP));
                        startActivity(call);
                    }
                });
            }
        });
    }

    private void initFirebase() {
        DatabaseReference detailDosen = rootRef.child("detaildosen").child(KEY_JURUSAN).child(KEY_DOSEN);
        detailDosen.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    DetailModel detailModel = dataSnapshot.getValue(DetailModel.class);
                    nama.setText(detailModel.nama);
                    email.setText(detailModel.email);
                    nohp.setText(detailModel.hp);

                    alamat.setText(detailModel.alamat);
                    notelprumah.setText(detailModel.notelprumah);
                    ruang.setText(detailModel.ruang);

                    EMAIL = detailModel.email;
                    NO_HP = detailModel.hp;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initLayout() {
        nama = (TextView) findViewById(R.id.data_nama);
        nohp = (TextView) findViewById(R.id.data_nohp);
        email = (TextView) findViewById(R.id.data_email);
        alamat = (TextView) findViewById(R.id.data_alamat);
        notelprumah = (TextView) findViewById(R.id.data_notelprumah);
        ruang = (TextView) findViewById(R.id.data_ruang);

        ll_email = (LinearLayout) findViewById(R.id.layout_email);
        ll_call = (LinearLayout) findViewById(R.id.layout_call);
    }
}
