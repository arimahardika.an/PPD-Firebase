package com.ridims.ppdfirebase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ridims.ppdfirebase.Helper.Static;

/**
 * Created by
 * Name : Ari Mahardika Ahmad Nafis
 * Email : arimahardika.an@gmail.com
 * Git : gitlab.com/Ari.Mahardika
 * Date : 17/10/16
 */

public class ListDosenActivity extends AppCompatActivity {

    String JURUSAN;
    private ListView dosenListView;
    private DatabaseReference rootref = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_dosen);

        Intent i = getIntent();
        JURUSAN = i.getStringExtra(Static.getJurusanKey());

        dosenListView = (ListView) findViewById(R.id.list_dosen);

        initFirebase();

    }

    private void initFirebase() {
        DatabaseReference listdosenref = rootref.child("listdosen").child(JURUSAN);
        FirebaseListAdapter<String> dosenAdapter = new FirebaseListAdapter<String>(this, String.class,
                android.R.layout.simple_list_item_1, listdosenref) {
            @Override
            protected void populateView(View v, String model, final int position) {
                ((TextView) v.findViewById(android.R.id.text1)).setText(model);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String key = getRef(position).getKey();

                        Intent i = new Intent(ListDosenActivity.this, DetailActivity.class);
                        i.putExtra(Static.getDosenKey(), key);
                        i.putExtra(Static.getJurusanKey(), JURUSAN);
                        startActivity(i);

                        System.out.println("ini keynya " + key);
                        System.out.println("ini jurusannya " + JURUSAN);
                    }
                });
            }
        };
        dosenListView.setAdapter(dosenAdapter);
    }
}
